
package com.proyecto.tiendaO3801.service;

import com.proyecto.tiendaO3801.modelo.Producto;
import java.util.List;

public interface ProductoService {
    public Producto save(Producto producto);
    public void delete(Integer id);
    public Producto findById(Integer id);
    public List<Producto> findAll();
}
