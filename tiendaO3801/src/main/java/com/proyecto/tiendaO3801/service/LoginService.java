
package com.proyecto.tiendaO3801.service;

import com.proyecto.tiendaO3801.modelo.Login;
import java.util.List;


public interface LoginService {
    public Login save(Login login);
    public void delete(Integer id);
    public Login findById(Integer id);
    public List<Login> findAll();
}
