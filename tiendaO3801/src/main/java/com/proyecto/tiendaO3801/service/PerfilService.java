package com.proyecto.tiendaO3801.service;

import com.proyecto.tiendaO3801.modelo.Perfil;
import java.util.List;

public interface PerfilService {

    public Perfil save(Perfil perfil);
    public void delete(Integer id);
    public Perfil findById(Integer id);
    public List<Perfil> findAll();
}
