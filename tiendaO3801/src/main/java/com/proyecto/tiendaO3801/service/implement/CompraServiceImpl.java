package com.proyecto.tiendaO3801.service.implement;

import com.proyecto.tiendaO3801.dao.CompraDAO;
import com.proyecto.tiendaO3801.modelo.Compra;
import com.proyecto.tiendaO3801.service.CompraService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CompraServiceImpl implements CompraService {

    @Autowired
    private CompraDAO compraDao;

    @Override
    @Transactional(readOnly = false)
    public Compra save(Compra compra) {
        return compraDao.save(compra);
    }

    @Override
    @Transactional(readOnly = false)
    public void delete(Integer id) {
        compraDao.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public Compra findById(Integer id) {
        return compraDao.findById(id).orElse(null);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Compra> findAll() {
        return (List<Compra>) compraDao.findAll();
    }
}
