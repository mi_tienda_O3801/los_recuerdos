package com.proyecto.tiendaO3801.service.implement;

import com.proyecto.tiendaO3801.dao.LoginDAO;
import com.proyecto.tiendaO3801.modelo.Login;
import com.proyecto.tiendaO3801.service.LoginService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class LoginServiceImpl implements LoginService {

    @Autowired
    private LoginDAO loginDao;

    @Override
    @Transactional(readOnly = false)
    public Login save(Login login) {
        return loginDao.save(login);
    }

    @Override
    @Transactional(readOnly = false)
    public void delete(Integer id) {
        loginDao.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public Login findById(Integer id) {
        return loginDao.findById(id).orElse(null);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Login> findAll() {
        return (List<Login>) loginDao.findAll();
    }
}
