package com.proyecto.tiendaO3801.service.implement;

import com.proyecto.tiendaO3801.dao.DetalleDAO;
import com.proyecto.tiendaO3801.modelo.Detalle;
import com.proyecto.tiendaO3801.service.DetalleService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class DetalleServiceImpl implements DetalleService {

    @Autowired
    private DetalleDAO detalleDao;

    @Override
    @Transactional(readOnly = false)
    public Detalle save(Detalle detalle) {
        return detalleDao.save(detalle);
    }

    @Override
    @Transactional(readOnly = false)
    public void delete(Integer id) {
        detalleDao.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public Detalle findById(Integer id) {
        return detalleDao.findById(id).orElse(null);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Detalle> findAll() {
        return (List<Detalle>) detalleDao.findAll();
    }
}
