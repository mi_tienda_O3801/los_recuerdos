package com.proyecto.tiendaO3801.service;

import com.proyecto.tiendaO3801.modelo.Usuario;
import java.util.List;

public interface UsuarioService {

    public Usuario save(Usuario usuario);
    public void delete(Integer id);
    public Usuario findById(Integer id);
    public List<Usuario> findAll();
}
