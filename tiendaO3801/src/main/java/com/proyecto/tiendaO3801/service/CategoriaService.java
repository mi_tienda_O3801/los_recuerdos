package com.proyecto.tiendaO3801.service;


import com.proyecto.tiendaO3801.modelo.Categoria;
import java.util.List;

public interface CategoriaService {

    public Categoria save(Categoria categoria);
    public void delete(Integer id);
    public Categoria findById(Integer id);
    public List<Categoria> findAll();
}
