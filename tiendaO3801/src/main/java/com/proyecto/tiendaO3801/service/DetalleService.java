package com.proyecto.tiendaO3801.service;

import com.proyecto.tiendaO3801.modelo.Detalle;
import java.util.List;

public interface DetalleService {

    public Detalle save(Detalle detalle);
    public void delete(Integer id);
    public Detalle findById(Integer id);
    public List<Detalle> findAll();
}
