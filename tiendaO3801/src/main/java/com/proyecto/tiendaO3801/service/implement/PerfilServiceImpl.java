package com.proyecto.tiendaO3801.service.implement;

import com.proyecto.tiendaO3801.dao.PerfilDAO;
import com.proyecto.tiendaO3801.modelo.Perfil;
import com.proyecto.tiendaO3801.service.PerfilService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class PerfilServiceImpl implements PerfilService {

    @Autowired
    private PerfilDAO perfilDao;

    @Override
    @Transactional(readOnly = false)
    public Perfil save(Perfil perfil) {
        return perfilDao.save(perfil);
    }

    @Override
    @Transactional(readOnly = false)
    public void delete(Integer id) {
        perfilDao.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public Perfil findById(Integer id) {
        return (Perfil) perfilDao.findById(id).orElse(null);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Perfil> findAll() {
        return (List<Perfil>) perfilDao.findAll();
    }
}
