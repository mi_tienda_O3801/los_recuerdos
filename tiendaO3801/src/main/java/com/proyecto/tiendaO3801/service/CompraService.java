
package com.proyecto.tiendaO3801.service;

import com.proyecto.tiendaO3801.modelo.Compra;
import java.util.List;


public interface CompraService {
    public Compra save(Compra compra);
    public void delete(Integer id);
    public Compra findById(Integer id);
    public List<Compra> findAll();
}
