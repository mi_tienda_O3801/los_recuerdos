package com.proyecto.tiendaO3801.service.implement;

import com.proyecto.tiendaO3801.dao.CategoriaDAO;
import com.proyecto.tiendaO3801.modelo.Categoria;
import com.proyecto.tiendaO3801.service.CategoriaService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CategoriaServiceImpl implements CategoriaService {

    @Autowired
    private CategoriaDAO categoriaDao;

    @Override
    @Transactional(readOnly = false)
    public Categoria save(Categoria categoria) {
        return categoriaDao.save(categoria);
    }

    @Override
    @Transactional(readOnly = false)
    public void delete(Integer id) {
        categoriaDao.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public Categoria findById(Integer id) {
        return (Categoria) categoriaDao.findById(id).orElse(null);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Categoria> findAll() {
        return (List<Categoria>) categoriaDao.findAll();
    }
}
