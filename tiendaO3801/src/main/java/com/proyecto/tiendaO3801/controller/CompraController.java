package com.proyecto.tiendaO3801.controller;

import com.proyecto.tiendaO3801.modelo.Compra;
import com.proyecto.tiendaO3801.service.CompraService;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("*")
@RequestMapping("/compra")
public class CompraController {

    @Autowired
    private CompraService compraservice;

    @PostMapping(value = "/")
    public ResponseEntity<Compra> agregar(@RequestBody Compra compra) {
        Compra obj = compraservice.save(compra);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    @DeleteMapping(value = "/list/{id}")
    public ResponseEntity<Compra> eliminar(@PathVariable Integer id) {
        Compra obj = compraservice.findById(id);
        if (obj != null) {
            compraservice.delete(id);
        } else {
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    @PutMapping(value = "/list/{id}")
    public ResponseEntity<Compra> editar(@RequestBody Compra compra) {
        Compra obj = compraservice.findById(compra.getIdCompra());
        if (obj != null) {
            obj.setVendedor(compra.getVendedor());
            obj.setCliente(compra.getCliente());
            obj.setTotal(compra.getTotal());
            obj.setFecha(compra.getFecha());

            compraservice.save(obj);
        } else {
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    @GetMapping(value = "/list")
    public List<Compra> consultarTodo() {
        return compraservice.findAll();
    }

    @GetMapping(value = "/list/{id}")
    public Compra consultaPorId(@PathVariable Integer id) {
        return compraservice.findById(id);
    }

}
