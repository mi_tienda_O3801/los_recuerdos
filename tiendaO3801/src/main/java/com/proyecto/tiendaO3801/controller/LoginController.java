package com.proyecto.tiendaO3801.controller;

import com.proyecto.tiendaO3801.modelo.Login;
import com.proyecto.tiendaO3801.service.LoginService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("*")
@RequestMapping("/login")
public class LoginController {

    @Autowired
    private LoginService loginservice;

    @PostMapping(value = "/")
    public ResponseEntity<Login> agregar(@RequestBody Login login) {
        Login obj = loginservice.save(login);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    @DeleteMapping(value = "/list/{id}")
    public ResponseEntity<Login> eliminar(@PathVariable Integer id) {
        Login obj = loginservice.findById(id);
        if (obj != null) {
            loginservice.delete(id);
        } else {
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    @PutMapping(value = "/list/{id}")
    public ResponseEntity<Login> editar(@RequestBody Login login) {
        Login obj = loginservice.findById(login.getIdLogin());
        if (obj != null) {
            obj.setEmail(login.getEmail());
            obj.setPassword(login.getPassword());
            obj.setPerfil(login.getPerfil());
            loginservice.save(obj);
        } else {
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    @GetMapping(value = "/list")
    public List<Login> consultarTodo() {
        return loginservice.findAll();
    }

    @GetMapping(value = "/list/{id}")
    public Login consultaPorId(@PathVariable Integer id) {
        return loginservice.findById(id);
    }

}
