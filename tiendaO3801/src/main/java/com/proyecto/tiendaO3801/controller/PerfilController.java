package com.proyecto.tiendaO3801.controller;

import com.proyecto.tiendaO3801.modelo.Perfil;
import com.proyecto.tiendaO3801.service.PerfilService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("*")
@RequestMapping("/perfil")
public class PerfilController {

    @Autowired
    private PerfilService perfilservice;

    @PostMapping(value = "/")
    public ResponseEntity<Perfil> agregar(@RequestBody Perfil perfil) {
        Perfil obj = perfilservice.save(perfil);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    @DeleteMapping(value = "/list/{id}")
    public ResponseEntity<Perfil> eliminar(@PathVariable Integer id) {
        Perfil obj = perfilservice.findById(id);
        if (obj != null) {
            perfilservice.delete(id);
        } else {
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    @PutMapping(value = "/list/{id}")
    public ResponseEntity<Perfil> editar(@RequestBody Perfil perfil) {
        Perfil obj = perfilservice.findById(perfil.getIdPerfil());
        if (obj != null) {
            obj.setPerfil(perfil.getPerfil());
            perfilservice.save(obj);
        } else {
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    @GetMapping("/list")
    public List<Perfil> consultarTodo() {
        return perfilservice.findAll();
    }

    @GetMapping("/list/{id}")
    public Perfil consultaPorId(@PathVariable Integer id) {
        return perfilservice.findById(id);
    }

}
