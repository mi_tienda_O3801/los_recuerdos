package com.proyecto.tiendaO3801.controller;

import com.proyecto.tiendaO3801.modelo.Categoria;
import com.proyecto.tiendaO3801.service.CategoriaService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("*")
@RequestMapping("/categoria")
public class CategoriasController {

    @Autowired
    private CategoriaService categoriaservice;

    @PostMapping(value = "/")
    public ResponseEntity<Categoria> agregar(@RequestBody Categoria categoria) {
        Categoria obj = categoriaservice.save(categoria);
        System.out.println("categoria add " + obj.getId());
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    @DeleteMapping(value = "/list/{id}")
    public ResponseEntity<Categoria> eliminar(@PathVariable Integer id) {
        Categoria obj = categoriaservice.findById(id);
        if (obj != null) {
            categoriaservice.delete(id);
        } else {
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    @PutMapping(value = "/list/{id}")
    public ResponseEntity<Categoria> editar(@RequestBody Categoria categoria) {
        Categoria obj = categoriaservice.findById(categoria.getId());
        if (obj != null) {
            obj.setCategorias(categoria.getCategorias());
            obj.setAumento(categoria.getAumento());
            categoriaservice.save(obj);
        } else {
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    @GetMapping("/list")
    public List<Categoria> consultarTodo() {
        return categoriaservice.findAll();
    }

    @GetMapping("/list/{id}")
    public Categoria consultaPorId(@PathVariable Integer id) {
        return categoriaservice.findById(id);
    }

}
