package com.proyecto.tiendaO3801.controller;

import com.proyecto.tiendaO3801.modelo.Detalle;
import com.proyecto.tiendaO3801.service.DetalleService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("*")
@RequestMapping("/detalle")
public class DetalleController {

    @Autowired
    private DetalleService detalleservice;

    @PostMapping(value = "/")
    public ResponseEntity<Detalle> agregar(@RequestBody Detalle detalle) {
        Detalle obj = detalleservice.save(detalle);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    @DeleteMapping(value = "/list/{id}")
    public ResponseEntity<Detalle> eliminar(@PathVariable Integer id) {
        Detalle obj = detalleservice.findById(id);
        if (obj != null) {
            detalleservice.delete(id);
        } else {
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    @PutMapping(value = "/list/{id}")
    public ResponseEntity<Detalle> editar(@RequestBody Detalle detalle) {
        Detalle obj = detalleservice.findById(detalle.getIdDetalle());
        if (obj != null) {
            obj.setCodigoproducto(detalle.getCodigoproducto());
            obj.setIdcompra(detalle.getIdcompra());
            obj.setValorUnd(detalle.getValorUnd());
            obj.setCantidad(detalle.getCantidad());
            obj.setTotalPagado(detalle.getTotalPagado());
            detalleservice.save(obj);
        } else {
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    @GetMapping(value = "/list")
    public List<Detalle> consultarTodo() {
        return detalleservice.findAll();
    }

    @GetMapping(value = "/list/{id}")
    public Detalle consultaPorId(@PathVariable Integer id) {
        return detalleservice.findById(id);
    }
}