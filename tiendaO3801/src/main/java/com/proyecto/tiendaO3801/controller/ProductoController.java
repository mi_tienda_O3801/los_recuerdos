package com.proyecto.tiendaO3801.controller;

import com.proyecto.tiendaO3801.modelo.Producto;
import com.proyecto.tiendaO3801.service.ProductoService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("*")
@RequestMapping("/producto")
public class ProductoController {

    @Autowired
    private ProductoService productoService;

    @PostMapping(value = "/")
    public ResponseEntity<Producto> agregar(@RequestBody Producto producto) {
        Producto obj = productoService.save(producto);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    @DeleteMapping(value = "/list/{id}")
    public ResponseEntity<Producto> eliminar(@PathVariable Integer id) {
        Producto obj = productoService.findById(id);
        if (obj != null) {
            productoService.delete(id);
        } else {
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    @PutMapping(value = "/list/{id}")
    public ResponseEntity<Producto> editar(@RequestBody Producto producto) {
        Producto obj = productoService.findById(producto.getIdProducto());
        if (obj != null) {
            obj.setCantidad(producto.getCantidad());
            obj.setNombreProducto(producto.getNombreProducto());
            obj.setCategoria(producto.getCategoria());
            obj.setValorCompra(producto.getValorCompra());
            obj.setPvp(producto.getPvp());
            obj.setCantidad(producto.getCantidad());
            productoService.save(obj);
        } else {
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    @GetMapping(value = "/list")
    public List<Producto> consultarTodo() {
        return productoService.findAll();
    }

    @GetMapping(value = "/list/{id}")
    public Producto consultaPorId(@PathVariable Integer id) {
        return productoService.findById(id);
    }

}
