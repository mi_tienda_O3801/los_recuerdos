
package com.proyecto.tiendaO3801.dao;

import com.proyecto.tiendaO3801.modelo.Perfil;
import org.springframework.data.repository.CrudRepository;


public interface PerfilDAO extends CrudRepository<Perfil, Integer>{
    
}
