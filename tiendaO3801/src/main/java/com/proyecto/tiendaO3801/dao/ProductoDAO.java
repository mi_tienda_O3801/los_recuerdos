
package com.proyecto.tiendaO3801.dao;

import com.proyecto.tiendaO3801.modelo.Producto;
import org.springframework.data.repository.CrudRepository;


public interface ProductoDAO extends CrudRepository<Producto, Integer>{
    
}
