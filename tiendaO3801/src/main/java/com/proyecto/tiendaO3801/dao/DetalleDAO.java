package com.proyecto.tiendaO3801.dao;

import com.proyecto.tiendaO3801.modelo.Detalle;
import org.springframework.data.repository.CrudRepository;

public interface DetalleDAO extends CrudRepository<Detalle, Integer> {

}
