package com.proyecto.tiendaO3801.dao;

import com.proyecto.tiendaO3801.modelo.Compra;
import org.springframework.data.repository.CrudRepository;

public interface CompraDAO extends CrudRepository<Compra, Integer> {

}
