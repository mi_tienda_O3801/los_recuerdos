
package com.proyecto.tiendaO3801.dao;

import com.proyecto.tiendaO3801.modelo.Categoria;
import org.springframework.data.repository.CrudRepository;


public interface CategoriaDAO extends CrudRepository<Categoria, Integer>{
    
}
