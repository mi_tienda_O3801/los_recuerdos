
package com.proyecto.tiendaO3801.dao;

import com.proyecto.tiendaO3801.modelo.Usuario;
import org.springframework.data.repository.CrudRepository;


public interface UsuarioDAO extends CrudRepository<Usuario, Integer>{
    
}
