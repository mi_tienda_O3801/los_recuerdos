
package com.proyecto.tiendaO3801.dao;

import com.proyecto.tiendaO3801.modelo.Login;
import org.springframework.data.repository.CrudRepository;


public interface LoginDAO extends CrudRepository<Login, Integer>{
    
}
