
package com.proyecto.tiendaO3801.modelo;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "detalle")
public class Detalle implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idDetalle")
    private Integer idDetalle;

    @ManyToOne
    @JoinColumn(name = "codigoProducto")
    private Producto codigoproducto;
    
    @ManyToOne
    @JoinColumn(name = "idCompra")
    private Compra idcompra;
    

    @Column(name = "valorUnd")
    private double valorUnd;

    @Column(name = "cantidadCompra")
    private double cantidad;
    
    @Column(name = "totalPagado")
    private double totalPagado;

    public Detalle() {
    }

    public Integer getIdDetalle() {
        return idDetalle;
    }

    public void setIdDetalle(Integer idDetalle) {
        this.idDetalle = idDetalle;
    }

    public Producto getCodigoproducto() {
        return codigoproducto;
    }

    public void setCodigoproducto(Producto codigoproducto) {
        this.codigoproducto = codigoproducto;
    }

    public Compra getIdcompra() {
        return idcompra;
    }

    public void setIdcompra(Compra idcompra) {
        this.idcompra = idcompra;
    }

    

    public double getValorUnd() {
        return valorUnd;
    }

    public void setValorUnd(double valorUnd) {
        this.valorUnd = valorUnd;
    }

    public double getCantidad() {
        return cantidad;
    }

    public void setCantidad(double cantidad) {
        this.cantidad = cantidad;
    }

    public double getTotalPagado() {
        return totalPagado;
    }

    public void setTotalPagado(double totalPagado) {
        this.totalPagado = totalPagado;
    }
    
    

}
